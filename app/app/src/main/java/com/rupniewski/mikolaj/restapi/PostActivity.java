package com.rupniewski.mikolaj.restapi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rupniewski.mikolaj.restapi.model.Post;
import com.rupniewski.mikolaj.restapi.remote.APIUtils;
import com.rupniewski.mikolaj.restapi.remote.PostService;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends AppCompatActivity {

    PostService postService;

    EditText edtPostId;
    EditText edtPostTitle;
    EditText edtPostBody;
    Button btnBack;
    Button btnSave;
    Button btnDel;
    TextView txtPostId;
    TextView txtPostTitle;
    TextView txtPostBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        init();


    }

    private void init() {
        edtPostId = findViewById(R.id.edtPostId);
        edtPostTitle = findViewById(R.id.edtPostTitle);
        edtPostBody = findViewById(R.id.edtPostBody);
        btnBack = findViewById(R.id.btnBack);
        btnSave = findViewById(R.id.btnSave);
        btnDel = findViewById(R.id.btnDel);
        txtPostId = findViewById(R.id.textPostId);
        txtPostTitle = findViewById(R.id.textPostTitle);
        txtPostBody = findViewById(R.id.textPostBody);

        postService = APIUtils.getPostService();

        Bundle extras = getIntent().getExtras();
        final String postId = extras.getString("post_id");
        String postTitle = extras.getString("post_title");
        String postBody = extras.getString("post_body");

        edtPostId.setText(postId);
        edtPostTitle.setText(postTitle);
        edtPostBody.setText(postBody);
        if(postId != null && postId.trim().length() > 0){
            edtPostId.setFocusable(false);
        } else {

            txtPostId.setVisibility(View.INVISIBLE);
            edtPostId.setVisibility(View.INVISIBLE);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Post post = new Post();
                post.setTitle(edtPostTitle.getText().toString());
                post.setBody(edtPostBody.getText().toString());
                if(postId != null && postId.trim().length() > 0) {
                    updatePost(Integer.parseInt(postId), post);
                } else {
                    addPost(post);
                }
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePost(Integer.parseInt(postId));

                Intent intent = new Intent(PostActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void addPost(Post post) {
        System.out.println(post);
        Call<Post> call = postService.addPost(post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    Toast.makeText(PostActivity.this,"Post created!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e("Error: ", t.getMessage());
            }
        });
    }
    public void updatePost(int id, final Post post) {
        post.setId(id);
        System.out.println(post);
        Call<Post> call = postService.updatePost(id, post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    Toast.makeText(PostActivity.this,"Post updated!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

                Log.e("Error: ", t.getMessage());
            }
        });
    }
    public void deletePost(int id) {
        Call<Post> call = postService.deletePost(id);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if(response.isSuccessful()){
                    Toast.makeText(PostActivity.this,"Post deleted!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e("Error: ", t.getMessage());
            }
        });
    }
}
