package com.rupniewski.mikolaj.restapi.remote;

public class APIUtils {

    private APIUtils(){
    };

    public static final String API_URL = "http://192.168.8.102:8080/api/";

    public static PostService getPostService(){
        return RetrofitClient.getClient(API_URL).create(PostService.class);
    }
}
