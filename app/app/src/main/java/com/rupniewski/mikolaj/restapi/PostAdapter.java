package com.rupniewski.mikolaj.restapi;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rupniewski.mikolaj.restapi.model.Post;

import java.util.List;

import androidx.annotation.NonNull;

public class PostAdapter extends ArrayAdapter<Post> {

    private Context context;
    private List<Post> posts;

    public PostAdapter(@NonNull Context context, int resource, @NonNull List<Post> objects) {
        super(context, resource, objects);
        this.context = context;
        this.posts = objects;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_post, parent, false);

        TextView txtPostId = rowView.findViewById(R.id.txtPostId);
        TextView txtPostTitle = rowView.findViewById(R.id.txtPostTitle);
        TextView txtPostBody = rowView.findViewById(R.id.txtPostBody);

        txtPostId.setText(String.format("post_id: %d", posts.get(pos).getId()));
        txtPostTitle.setText(String.format("post_title: %s", posts.get(pos).getTitle()));
        txtPostBody.setText(String.format("post_body: %s", posts.get(pos).getBody()));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("post_id", String.valueOf(posts.get(pos).getId()));
                intent.putExtra("post_title", posts.get(pos).getTitle());
                intent.putExtra("post_body", posts.get(pos).getBody());
                context.startActivity(intent);
            }
        });
        return rowView;
    }
}
