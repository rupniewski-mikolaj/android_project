package com.rupniewski.mikolaj.restapi;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.rupniewski.mikolaj.restapi.model.Post;
import com.rupniewski.mikolaj.restapi.remote.APIUtils;
import com.rupniewski.mikolaj.restapi.remote.PostService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btnAddPost;
    Button btnGetPosts;
    ListView listViewPosts;

    PostService postService;
    List<Post> listPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        btnAddPost = findViewById(R.id.btnAddPost);
        btnGetPosts = findViewById(R.id.btnGetPosts);
        listViewPosts = findViewById(R.id.listViewPosts);
        listPosts = new ArrayList<>();
        postService = APIUtils.getPostService();

        btnGetPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPostList();
            }
        });
        btnAddPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PostActivity.class);
                intent.putExtra("post_title", "");
                intent.putExtra("post_body", "");
                startActivity(intent);
            }
        });
    }
    public void getPostList() {
        Call<List<Post>> call = postService.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    listPosts = response.body();
                    listViewPosts.setAdapter(new PostAdapter(MainActivity.this, R.layout.list_post, listPosts));
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e("Error: ", t.getMessage());
            }
        });
    }
}
