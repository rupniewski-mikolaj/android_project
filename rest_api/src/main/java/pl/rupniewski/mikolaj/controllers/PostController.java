package pl.rupniewski.mikolaj.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.rupniewski.mikolaj.exception.ResourceNotFoundException;


import pl.rupniewski.mikolaj.models.Post;
import pl.rupniewski.mikolaj.repository.PostRepository;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PostController {

    @Autowired
    PostRepository postRepository;

    @GetMapping("/posts")
    public List<Post> getAllCategories() {
        return postRepository.findAll();
    }

    @PostMapping("/posts")
    public Post crateCategory(@Valid @RequestBody Post category) {
        return postRepository.save(category);
    }

    // Get a Single Category
    @GetMapping("/posts/{id}")
    public Post getCategoryById(@PathVariable(value = "id") Long categoryId) {
        return postRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Post", "id", categoryId));
    }
    // Update a Category
    @PutMapping("/posts/{id}")
    public Post updateCategory(@PathVariable(value = "id") Long categoryId,
                           @Valid @RequestBody Post categoryDetails) {

        Post post = postRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));

        post.setTitle(categoryDetails.getTitle());
        post.setBody(categoryDetails.getBody());


        Post updatedNote = postRepository.save(post);
        return updatedNote;
    }
    // Delete a Category
    @DeleteMapping("/posts/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long categoryId) {
        Post post = postRepository.findById(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));

        postRepository.delete(post);

        return ResponseEntity.ok().build();
    }
}
