package pl.rupniewski.mikolaj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.rupniewski.mikolaj.models.Post;

public interface PostRepository extends JpaRepository <Post, Long> {
    Post findByTitle(String title);

}
